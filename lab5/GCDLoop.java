public class GCDLoop{

 

 

    public static void main(String[] args){

      

       int x =Integer.parseInt(args[0]);

       int y=Integer.parseInt(args[1]);

 

       int c=gcd(x,y);

       System.out.println(c);

    }

   

 

    public static int gcd(int x, int y){

        int r=0;

   

        if(x>y){

           

            do{

                r= x%y;

                if(r==0){

                    return y;

                   

                }

                else{

                    x=y;

                    y=r;

                   

                }

      

            } while(r!=0);

    

        }

        else if(x<y){

           

            do{

                r= y%x;

                if(r==0){

                    return x;

                   

                }

                else{

                    y=x;

                    x=r;

                    continue;

                }

 

            }while(r!=0);

        }

       

        return x;

       

        

    }  

        

}
