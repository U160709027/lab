package exam;

public class Q6 {
	int num;

	public Q6(int num) {
		this.num = ++num;
	}

	public static void main(String[] args) {
		int num1 = 4;
		int num2 = num1;
		Q6 qA = new  Q6(num1);
		Q6 qB = qA;
		qB.num = qA.num + qA.num;
		String str = "Hello";
		qB = foo(qB, --num2, ++num1, str);
		System.out.println(num1 + ", " + num2 + ", " + qA.num + ", " + qB.num + ", " + str);
	}

	public static Q6 foo(Q6 q, int num1, int num2, String str) {
		str = "Hola";
		q.num = 20 + ++num1 + num2--;
		q = new Q6(num2);
		return q;
	}
}

