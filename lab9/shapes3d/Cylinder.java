import shapes2d.Circle;

public class Cylinder extends Circle{

	private int height;

	public Cylinder(double radius,int height){


		super(radius);
		this.height=height;
	}

	public double volume(){

		return height*super.area();
	}

	public double area(){

		return super.Area()*2+2*Math.PI*radius*height ;

	}

	public String toString(){

  		return "height = " +height;

}


}
