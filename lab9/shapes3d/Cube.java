public class Cube extends Square {

	public Cube(int side){

		super(side);
	}


	public int area(){

		return super.area*side*6;
	}

	public int volume(){

		return side*side*side;
	}

	public String toString(){

		return "side = " +side;
}
}
