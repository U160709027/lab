public class Rectangle{


	int sideA;
	int sideB;
	Point topLeft;

	public Rectangle(int sideA,int sideB, Point topLeft){


		this.sideA=sideA;
		this.sideB=sideB;
		this.topLeft=topLeft;



	}

	public int area(){

		return this.sideA * this.sideB;

	}

	public  int perimeter(){


		return 2*(this.sideA + this.sideB);

	}

	public Point[]   corners(){

		Point[] corners = new Point[4];
		corners[0]=this.topLeft;
		corners[1]=new Point(this.topLeft.xCoord + sideA,this.topLeft.yCoord);
		corners[2]=new Point(this.topLeft.xCoord ,this.topLeft.yCoord + sideB);
		corners[3]=new Point(this.topLeft.xCoord + sideA,this.topLeft.yCoord + sideB);
		return corners;


	}



}
