public class Main{


	public static void main(String[] args){


		Rectangle r = new Rectangle(5,8,new Point(6,10));
		System.out.println(r.area());
		System.out.println(r.perimeter());
		System.out.println(r.corners());
		Point [] pa = r.corners();
		for(int i=0;i<pa.length;i++){
			System.out.println(pa[i].xCoord + " " + pa[i].yCoord);
		}


		Circle c = new Circle(10,new Point(6,6));
		System.out.println(c.area());
		System.out.println(c.perimeter());
		System.out.println(c.overlap(new Circle(5,new Point(6,6))));

	}




}
